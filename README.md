# starter-vue2-custom-002

## Project setup

```
yarn install
```

### Compiles and hot-reloads for development

```
yarn serve
```

### Compiles and minifies for production

```
yarn build
```

### Run your unit tests

```
yarn test:unit
```

### Lints and fixes files

```
yarn lint
```

### Customize configuration

See [Configuration Reference](https://cli.vuejs.org/config/).

### lancer le server local des données test ( todo )

```
json-server --watch src/assets/mocks/todos.json --port 5151
```

#####

https://commondatastorage.googleapis.com/gtv-videos-bucket/sample/
BigBuckBunny.mp4
